package com.rozetka.applogic;

public interface INavigationHelper {

    void navigateToMainPage();

    void openRelativeUrl(String url);

    void switchToNewTab();

    void refreshPage();

    void navigateToCreditPage();

    void navigateToProductGroupedPage();

    void navigateToProductListPage();

    void navigateToProductPage();

    void navigateToRegistrationPage();

    void navigateToSearchResultPage(String product);

    void navigateToProductListPagePrice10000_12999Filter();
}
