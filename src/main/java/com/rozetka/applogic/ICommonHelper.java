package com.rozetka.applogic;

import com.rozetka.applogic2.CommonHelper;

public interface ICommonHelper {
    public int sizeOfListCreditRules();

    public Boolean isCreditTermsTableTitleDisplayed();

    public String getValueFromCreditTable(String packetName, String columnName);

    public String getAgeBorrowerAlphaBank();

    public CommonHelper changeViewToList();

    public int sizeOfProductListViewList();

    public void gMoreProducts();

    public int sizeOfProductListViewTile();

    public boolean isPrice10000_12999FilterSelected();

}
