package com.rozetka.applogic;

import com.rozetka.applogic2.CommonHelper;
import com.rozetka.applogic2.NavigationHelper;
import com.rozetka.applogic2.ProductHelper;
import com.rozetka.applogic2.UserHelper;
import org.openqa.selenium.WebDriver;

public interface IApplicationManager {

    WebDriver getWebDriver();

    String getBaseUrl();

    UserHelper getUserHelper();

    ProductHelper getProductHelper();

    NavigationHelper getNavigationHelper();

    CommonHelper getCommonHelper();

    void stop();

    void close();
}
