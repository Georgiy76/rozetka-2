package com.rozetka.applogic;

public interface IProductHelper {

    boolean isBasketEmpty();

    void addFirstProductToBasket();

    boolean isProductAddedToBasket();
}
