package com.rozetka.applogic;

import com.rozetka.model.User;

public interface IUserHelper {

    void createNewUser(User user);

    void logInAs(User user);

    void logInAs(User user, boolean loginWithPhoneNumber);

    void logOut();

    void logOut(User user);

    boolean isLoggedIn();

    boolean isLoggedInAs(User user);

    boolean isNotLoggedIn();

    boolean checkAuthorization(User user);

}
