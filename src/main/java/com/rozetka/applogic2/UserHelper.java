package com.rozetka.applogic2;

import com.rozetka.applogic.IUserHelper;
import com.rozetka.model.User;

public class UserHelper extends DriverBasedHelper implements IUserHelper {

    public UserHelper(ApplicationManager manager) {
        super(manager.getWebDriver());
    }

    @Override
    public void logInAs(User user, boolean loginWithPhoneNumber) {
        pages.anyPage.ensurePageLoaded()
                .clickOnOpenSignINFormButton()
                .setToUsernameField(loginWithPhoneNumber ? user.getPhoneNumber() : user.getEmail())
                .setToPasswordField(user.getPassword())
                .clickSubmitSignINFormButton();
        user.setAuthorized(true);
    }

    @Override
    public void logInAs(User user) {
        pages.anyPage.ensurePageLoaded()
                .clickOnOpenSignINFormButton()
                .setToUsernameField(user.getEmail())
                .setToPasswordField(user.getPassword())
                .clickSubmitSignINFormButton();
        user.setAuthorized(true);
    }

    @Override
    public void logOut(User user) {
        pages.anyPage.clickOnLogoutButton();
        user.setAuthorized(false);
    }

    @Override
    public void logOut() {
        pages.anyPage.clickOnLogoutButton();
    }

    @Override
    public boolean isLoggedIn() {
        return pages.anyPage.isLoggedIn();
    }

    @Override
    public boolean isLoggedInAs(User user) {
        return pages.anyPage.isLoggedInAs(user);
    }

    @Override
    public boolean isNotLoggedIn() {
        return pages.anyPage.isNotLoggedIn();
    }

    @Override
    public boolean checkAuthorization(User user) {
        return pages.anyPage.IsDislayedSigninButton() && user.getISAuthorized();
    }

    @Override
    public void createNewUser(User user) {
        pages.anyPage.ensurePageLoaded().clickOnOpenRegistrationPage();
        pages.registrationPage.ensurePageLoaded()
                .setNameField(user.getName())
                .setEmailField(user.getEmail())
                .setPasswordField(user.getPassword())
                .submitRegistationFormButton();
        user.setAuthorized(true);
        pages.profilePage.ensurePageLoaded();
    }
}
