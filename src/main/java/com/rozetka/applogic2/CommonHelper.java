package com.rozetka.applogic2;

import com.rozetka.applogic.ICommonHelper;

public class CommonHelper extends DriverBasedHelper implements ICommonHelper {

    public CommonHelper(ApplicationManager manager) {
        super(manager.getWebDriver());
    }

    @Override
    public int sizeOfListCreditRules() {
        return pages.creditPage.sizeOfListCreditRules();
    }

    @Override
    public Boolean isCreditTermsTableTitleDisplayed() {
        return pages.creditPage.isCreditTermsTableTitleDisplayed();
    }

    @Override
    public String getValueFromCreditTable(String packetName, String columnName) {
        return pages.creditPage.getValueFromCreditTable(packetName, columnName);
    }

    @Override
    public String getAgeBorrowerAlphaBank() {
        return pages.creditPage.getAgeBorrowerAlphaBank();
    }

    @Override
    public CommonHelper changeViewToList() {
        pages.productListPage.changeViewToList();
        return this;
    }

    @Override
    public int sizeOfProductListViewList() {
        return pages.productListPage.sizeOfProductListViewList();
    }

    @Override
    public void gMoreProducts() {
        pages.productListPage.gMoreProducts();
    }

    @Override
    public int sizeOfProductListViewTile() {
        return pages.productListPage.sizeOfProductListViewTile();
    }

    @Override
    public boolean isPrice10000_12999FilterSelected() {
        return pages.productListPage.isPrice10000_12999FilterSelected();
    }


}
