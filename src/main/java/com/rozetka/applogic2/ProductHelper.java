package com.rozetka.applogic2;

import com.rozetka.applogic.IProductHelper;

public class ProductHelper extends DriverBasedHelper implements IProductHelper {

    public ProductHelper(ApplicationManager manager) {
        super(manager.getWebDriver());
    }

    @Override
    public boolean isBasketEmpty() {
        return pages.anyPage
                .openBasket()
                .isBasketEmpty();
    }

    @Override
    public void addFirstProductToBasket() {
        pages.searchResultPage
                .ensurePageLoaded()
                .addFirstProductToBasket();
    }

    @Override
    public boolean isProductAddedToBasket() {
        return pages.anyPage
                .ensurePageLoaded()
                .isProductAddedToBasket();
    }

    public boolean numberOfBrandsInMainList(int count) {
        return count == pages.productGroupedPage.sizeOfBrandsInMainList();
    }
}
