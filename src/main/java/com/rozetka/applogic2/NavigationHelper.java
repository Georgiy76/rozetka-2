package com.rozetka.applogic2;

import com.rozetka.applogic.INavigationHelper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

public class NavigationHelper extends DriverBasedHelper implements INavigationHelper {

    private String baseUrl;

    public NavigationHelper(ApplicationManager manager) {
        super(manager.getWebDriver());
        this.baseUrl = manager.getBaseUrl();
    }

    public void navigateToMainPage() {
        driver.get(baseUrl);
    }

    @Override
    public void openRelativeUrl(String url) {
        driver.get(baseUrl + url);
    }

    @Override
    public void switchToNewTab() {
        //Windows
        new Actions(driver)
                .sendKeys(Keys.chord(Keys.CONTROL, Keys.TAB))
                .perform();
    }

    @Override
    public void refreshPage() {
        super.driver.navigate().refresh();
    }

    @Override
    public void navigateToCreditPage() {
        pages.anyPage.openCreditLink();
    }

    @Override
    public void navigateToProductGroupedPage() {
        pages.anyPage.openProductGroupedPage();
    }

    @Override
    public void navigateToProductListPage() {
        navigateToProductGroupedPage();
        pages.productGroupedPage.openProductListPage();
    }

    @Override
    public void navigateToProductPage() {

    }

    @Override
    public void navigateToRegistrationPage() {

    }

    @Override
    public void navigateToSearchResultPage(String product) {
        pages.anyPage.ensurePageLoaded()
                .setSearchField(product)
                .submitSearch();
    }

    @Override
    public void navigateToProductListPagePrice10000_12999Filter() {
        navigateToProductGroupedPage();
        pages.productGroupedPage.selectPrice10000_12999Filter();
    }

    public String getTtitleOfPage() {
        return pages.anyPage.getTitle();
    }

}
