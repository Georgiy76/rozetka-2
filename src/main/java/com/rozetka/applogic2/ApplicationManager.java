package com.rozetka.applogic2;

import com.rozetka.applogic.IApplicationManager;
import com.rozetka.util.Browser;
import com.rozetka.util.PropertyLoader;
import com.rozetka.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class ApplicationManager implements IApplicationManager {

    private UserHelper userHelper;
    private ProductHelper productHelper;
    private NavigationHelper navigationHelper;
    private CommonHelper commonHelper;
    private String browserName;

    private WebDriver driver;

    private String baseUrl = "https://rozetka.com.ua/";

    public ApplicationManager() throws IOException {
        baseUrl = PropertyLoader.loadProperty("site.url");
        browserName = PropertyLoader.loadProperty("browser.name");
        String gridHubUrl = PropertyLoader.loadProperty("grid2.hub");

        Browser browser = new Browser();
        browser.setName(PropertyLoader.loadProperty("browser.name"));
        browser.setVersion(PropertyLoader.loadProperty("browser.version"));
        browser.setPlatform(PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        driver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password);

        userHelper = new UserHelper(this);
        productHelper = new ProductHelper(this);
        navigationHelper = new NavigationHelper(this);
        commonHelper = new CommonHelper(this);
    }

    @Override
    public UserHelper getUserHelper() {
        return userHelper;
    }

    @Override
    public ProductHelper getProductHelper() {
        return productHelper;
    }

    @Override
    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
    }

    @Override
    public CommonHelper getCommonHelper() {
        return commonHelper;
    }

    @Override
    public WebDriver getWebDriver() {
        return driver;
    }

    @Override
    public String getBaseUrl() {
        return baseUrl;
    }

    @Override
    public void stop() {
        if (driver != null) {
            driver.close();
        }
    }

    @Override
    public void close() {
    }
}
