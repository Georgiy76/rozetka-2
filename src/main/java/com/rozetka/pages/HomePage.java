package com.rozetka.pages;

import com.rozetka.pages.common.AnyPage;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class HomePage extends AnyPage {

    public HomePage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//div[@class='main-big-promo']")
    private WebElement slider;

    By bySlider = By.xpath("//div[@class='main-big-promo']");

    public HomePage ensurePageLoaded() {
        super.ensurePageLoaded();
        wait.until(ExpectedConditions.presenceOfElementLocated(bySlider));
        return this;
    }

}
