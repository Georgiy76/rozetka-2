package com.rozetka.pages.common;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElements;

public class Page {
    protected WebDriver driver;
    protected Actions actions;
    protected WebDriverWait wait;
    protected PageManager pages;

    public Page(PageManager pages) {
        this.pages = pages;
        driver = pages.getWebDriver();
        wait = new WebDriverWait(driver, 10);
        actions = new Actions(driver);
    }

    public WebDriver getWevDriver() {
        return driver;
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public Page navigateToElementJE(WebElement webElement) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
        return this;
    }

    public Page navigateToElement(WebElement webElement) {
        //actions.;
        return this;
    }

    public Page ensurePageLoaded() {
        return this;
    }

    public Page ensureElementIsVisible(WebElement webElement) {
        wait.until(visibilityOf(webElement));
        return this;
    }

    public Page ensureElementIsVisible(List<WebElement> webElements) {
        wait.until(visibilityOfAllElements(webElements));
        return this;
    }

    public boolean waitPageLoaded() {
        try {
            ensurePageLoaded();
            return true;
        } catch (TimeoutException to) {
            return false;
        }
    }
}
