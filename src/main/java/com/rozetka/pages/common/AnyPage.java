package com.rozetka.pages.common;

import com.rozetka.model.User;
import com.rozetka.pages.CreditPage;
import com.rozetka.pages.ProductGroupedPage;
import com.rozetka.pages.RegistrationPage;
import com.rozetka.pages.SearchResultPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class AnyPage extends Page {

    public AnyPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//*[@href='https://my.rozetka.com.ua/signup/']")
    private WebElement registrationButton;

    @FindBy(xpath = "//a[@href='https://my.rozetka.com.ua/signin/']")
    private WebElement signinFormButton;

    @FindBy(xpath = "//a[@href='https://my.rozetka.com.ua/']")
    private WebElement loggedMarker;

    @FindBy(xpath = "//*[@name='login']")
    private WebElement loginField;

    @FindBy(xpath = "//div[1]/input[@name='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//form[@id='popup_signin']//button")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id='user_menu']/span[1]")
    private WebElement welcome;

    @FindBy(xpath = "//span[@id='header_user_menu_parent']/a")
    private WebElement headerUserMenu;

    @FindBy(xpath = "//*[@id='header_user_menu']/li[12]/a")
    private WebElement logoutButton;

    @FindBy(xpath = "//input[@class='rz-header-search-input-text passive']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='btn-link-i js-rz-search-button']")
    private WebElement searchBatton;

    @FindBy(xpath = "//li[@class='hub-i hub-i-cart']/div[@name='splash-button']")
    private WebElement basketButton;

    @FindBy(xpath = "//*[contains(text(),'Корзина пуста')]")
    private WebElement basketIsEmptyTitle;

    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/credit/']")
    private WebElement creditPageLink;

    @FindBy(xpath = "//h2[contains(text(),'Вы добавили товар в корзину')]")
    private WebElement basketTitle;

    @FindBy(xpath = "//a[@name='close']")
    private WebElement closeCartPopup;

    @FindBy(xpath = "//span[contains(text(),'Каталог товаров')]")
    private WebElement productCatalogButton;

    @FindBy(xpath = "//a[@data-title='Ноутбуки и компьютеры']")
    private WebElement notebooksPCProducts;

    @FindBy(xpath = "//li[@class='f-menu-sub']/a[@href='https://rozetka.com.ua/notebooks/c80004/']")
    //li[@class='f-menu-pop-l-i']/a[@href='https://rozetka.com.ua/notebooks/c80004/']
    private WebElement notebooksButton;

    @FindBy(xpath = "//div[@id='cart-popup']")
    private WebElement basketSection;

    public AnyPage ensurePageLoaded() {
        return this;
    }

    public RegistrationPage clickOnOpenRegistrationPage() {
        registrationButton.click();
        return pages.registrationPage;
    }

    public AnyPage clickOnOpenSignINFormButton() {
        signinFormButton.click();
        return this;
    }

    public AnyPage setToUsernameField(String username) {
        loginField.sendKeys(username);
        return this;
    }

    public AnyPage setToPasswordField(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public AnyPage clickSubmitSignINFormButton() {
        loginButton.click();
        return pages.anyPage;
    }

    public AnyPage clickOnLogoutButton() {
        actions.moveToElement(headerUserMenu).moveToElement(logoutButton).click().perform();
        return pages.anyPage;
    }

    public boolean IsDislayedSigninButton() {
        return signinFormButton.isDisplayed();
    }

    public AnyPage setSearchField(String text) {
        searchField.sendKeys(text);
        return this;
    }

    public SearchResultPage submitSearch() {
        searchBatton.click();
        return pages.searchResultPage;
    }

    public AnyPage clickOnBasketButton() {
        basketButton.click();
        return pages.anyPage;
    }

    public AnyPage openBasket() {
        basketButton.click();
        return this;
    }

    public boolean isBasketEmpty() {
        ensureElementIsVisible(basketSection);
        return basketIsEmptyTitle.isDisplayed();
    }

    public boolean isProductAddedToBasket() {
        ensureElementIsVisible(basketSection);
        return basketTitle.isDisplayed();
    }

    public ProductGroupedPage openProductGroupedPage() {
        actions.moveToElement(productCatalogButton)
                .moveToElement(notebooksPCProducts)
                .perform();
        ensureElementIsVisible(notebooksButton);
        notebooksButton.click();
        return pages.productGroupedPage;
    }

    public boolean isLoggedIn() {
        return loggedMarker.isDisplayed();
    }

    public boolean isNotLoggedIn() {
        return signinFormButton.isDisplayed();
    }

    public boolean isLoggedInAs(User user) {
        By nameXpath = By.xpath("//a[contains(text(),'" + user.getName() + "')]");
        wait.until(visibilityOfElementLocated(nameXpath));
        return driver.findElements(nameXpath).size() > 0 && user.getISAuthorized();
    }

    public CreditPage openCreditLink() {
        creditPageLink.click();
        return pages.creditPage;
    }
}
