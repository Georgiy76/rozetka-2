package com.rozetka.pages.common;

import com.rozetka.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageManager {

    private WebDriver driver;

    public AnyPage anyPage;
    public HomePage homePage;
    public CreditPage creditPage;
    public ProductGroupedPage productGroupedPage;
    public ProductListPage productListPage;
    public ProductPage productPage;
    public ProfilePage profilePage;
    public RegistrationPage registrationPage;
    public SearchResultPage searchResultPage;

    public PageManager(WebDriver driver) {
        this.driver = driver;

        anyPage = initElements(new AnyPage(this));
        homePage = initElements(new HomePage(this));
        creditPage = initElements(new CreditPage(this));
        productGroupedPage = initElements(new ProductGroupedPage(this));
        productListPage = initElements(new ProductListPage(this));
        productPage = initElements(new ProductPage(this));
        profilePage = initElements(new ProfilePage(this));
        registrationPage = initElements(new RegistrationPage(this));
        searchResultPage = initElements(new SearchResultPage(this));
    }

    private <T extends Page> T initElements(T page) {
        PageFactory.initElements(driver, page);
        return page;
    }

    public WebDriver getWebDriver() {
        return driver;
    }


}
