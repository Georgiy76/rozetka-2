package com.rozetka.pages;

import com.rozetka.pages.common.Page;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductListPage extends Page {

    public ProductListPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//div[@class='over-wraper']")
    List<WebElement> productListViewTile;

    @FindBy(xpath = "//div[@class='g-i-list-middle-part']")
    List<WebElement> productListViewList;

    @FindBy(xpath = "//img[@alt='Списком']")
    WebElement viewList;

    //    @FindBy (xpath = "//a[@class='novisited g-i-more-link']")
    @FindBy(xpath = "//span[contains(text(),'Показать еще 16 товаров')]")
    WebElement gMoreProductsButton;

    @FindBy(xpath = "//a[contains(text(),'от 10000 до 12999 грн')]")
    WebElement price10000_12999Filter;

    @FindBy(xpath = "//div[@class='g-i-list-middle-part']")
    List<WebElement> byProductListViewList;

    @FindBy(xpath = "(//div[@class='g-i-list-middle-part'])[32]")
    List<WebElement> product32AtList;

    By byPrice10000_12999Filter = By.xpath("//a[contains(text(),'от 10000 до 12999 грн')]");

    public int sizeOfProductListViewTile() {
        return productListViewTile.size();
    }


    public void changeViewToList() {
        viewList.click();
        ensureElementIsVisible(productListViewList);
    }

    public int sizeOfProductListViewList() {
        return productListViewList.size();
    }

    public void gMoreProducts() {
        ensureElementIsVisible(gMoreProductsButton).navigateToElementJE(gMoreProductsButton);
        gMoreProductsButton.click();
        ensureElementIsVisible(product32AtList);
    }

    public boolean isPrice10000_12999FilterSelected() {
//        wait.until(ExpectedConditions.visibilityOf(price10000_12999Filter));
        return price10000_12999Filter.isDisplayed();
    }

    public void removeFilter10000_12999() {
        price10000_12999Filter.click();
        // wait.until(ExpectedConditions.stalenessOf(price10000_12999Filter));
    }
}
