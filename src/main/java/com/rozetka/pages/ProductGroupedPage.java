package com.rozetka.pages;

import com.rozetka.pages.common.Page;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductGroupedPage extends Page {

    public ProductGroupedPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//div[@name='block_with_goods'][@class='pab-cell pab-img-45']")
    List<WebElement> brandsList;

    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/notebooks/c80004/filter/producer=asus/']")
    WebElement brandAsus;

    @FindBy(xpath = "//a[contains(text(),'10 000 грн - 12 999 грн')]")
    WebElement price10000_12999FilterButton;

    public int sizeOfBrandsInMainList() {
        return brandsList.size();
    }

    public ProductListPage openProductListPage() {
        brandAsus.click();
        return pages.productListPage;
    }

    public ProductPage selectPrice10000_12999Filter() {
        price10000_12999FilterButton.click();
        return pages.productPage;
    }
}
