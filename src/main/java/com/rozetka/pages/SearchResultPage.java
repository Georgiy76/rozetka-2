package com.rozetka.pages;

import com.rozetka.pages.common.Page;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class SearchResultPage extends Page {

    public SearchResultPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "(//Button[@class='btn-link-i'])[1]")
    private WebElement addFirstProductToBasketButton;

    public SearchResultPage ensurePageLoaded() {
        By byFilterForm = By.xpath("//div[@id='parameters-filter-form']");
        super.ensurePageLoaded();
        wait.until(visibilityOfElementLocated(byFilterForm));
        return this;
    }

    public ProductPage addFirstProductToBasket() {
        addFirstProductToBasketButton.click();
        /*try {

        } catch (NullPointerException e) {
            System.out.println("List of products is empty");
        }*/
        return pages.productPage;
    }
}
