package com.rozetka.pages;

import com.rozetka.pages.common.Page;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class RegistrationPage extends Page {

    public RegistrationPage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "(//input[@class='input-text auth-input-text'])[1]")
    private WebElement nameField;

    @FindBy(xpath = "(//input[@class='input-text auth-input-text'])[2]")
    private WebElement emailField;

    @FindBy(xpath = "(//input[@class='input-text auth-input-text'])[3]")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@class='btn-link-i']")
    private WebElement submitRegistationFormButton;

    public RegistrationPage ensurePageLoaded() {
        By byBodyTitle = By.xpath("//h1[contains(text(),'Регистрация')]");
        super.ensurePageLoaded();
        wait.until(visibilityOfElementLocated(byBodyTitle));
        return this;
    }

    public RegistrationPage setNameField(String name) {
        nameField.sendKeys(name);
        return this;
    }

    public RegistrationPage setEmailField(String email) {
        emailField.sendKeys(email);
        return this;
    }

    public RegistrationPage setPasswordField(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public ProfilePage submitRegistationFormButton() {
        submitRegistationFormButton.submit();
        return pages.profilePage;
    }

}
