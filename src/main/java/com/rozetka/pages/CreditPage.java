package com.rozetka.pages;

import com.rozetka.pages.common.Page;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreditPage extends Page {

    public CreditPage(PageManager pages) {
        super(pages);
    }

    Map<String, String> columnNameMap = new HashMap<String, String>();

    @FindBy(xpath = "//div[@class='credit-rules-title']")
    List<WebElement> creditRuleList;

    //можно сделать короче, но тогда не будет проверки на то что текст находиться в тайтле таблицы
    @FindBy(xpath = "//div[@class='rz-credit-block']/h2[contains(text(),'Условия кредитования')]")
    WebElement creditTermsTableTitle;

    @FindBy(xpath = "(//table[@class='rz-credit-terms-table'])[1]//tr[@class='rz-credit-terms-tr']")
    List<WebElement> creditTermsTableBaseCell;

    @FindBy(xpath = "//img[@title='Альфа-Банк']//ancestor::td/following-sibling::*[4]")
    WebElement ageBorrowerRentAlphaBank;

    @FindBy(xpath = "//td[@class='rz-credit-terms-td rz-credit-terms-td-deposit']")
    List<WebElement> depositTermsList;

    public int sizeOfListCreditRules() {
        return creditRuleList.size();
    }

    public Boolean isCreditTermsTableTitleDisplayed() {
        return creditTermsTableTitle.isDisplayed();
    }

    public String getValueFromCreditTable(String packetName, String columnName) {
        int i = 0;
        fillColumnNameMap();

        for (WebElement webElement : depositTermsList) {
            i++;
            if (webElement.getText().contains(packetName))
                return driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='" + columnNameMap.get(columnName) + "'])[" + i + "]"))
                        .getText();

        }
        return "Something went wrong";
    }

    private void fillColumnNameMap() {
        columnNameMap.put("Льготный период без комиссий", "rz-credit-terms-td rz-credit-terms-td-period");
        columnNameMap.put("Годовая ставка", "rz-credit-terms-td rz-credit-terms-td-rate");
        columnNameMap.put("Ежемесячная комиссия после льготного периода", "rz-credit-terms-td rz-credit-terms-td-commision");
        columnNameMap.put("Разовая комиссия", "rz-credit-terms-td rz-credit-terms-td-single_commission");
        columnNameMap.put("Первоначальный взнос", "rz-credit-terms-td rz-credit-terms-td-contribution");
    }

    public String getAgeBorrowerAlphaBank() {
        //((Locatable)ageBorrowerRentAlphaBank).getCoordinates();
        //((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",ageBorrowerRentAlphaBank);
        actions = new Actions(driver);
        actions.moveToElement(ageBorrowerRentAlphaBank);
        actions.perform();

        System.out.println(ageBorrowerRentAlphaBank.getText());
        return ageBorrowerRentAlphaBank.getText();
    }

}
