package com.rozetka.pages;

import com.rozetka.pages.common.AnyPage;
import com.rozetka.pages.common.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;


public class ProfilePage extends AnyPage {

    public ProfilePage(PageManager pages) {
        super(pages);
    }

    @FindBy(xpath = "//button[contains(text(),'Подтвердить')]")
    WebElement confitmEmailButtonOnPopup;

    @FindBy(xpath = "//div[contains(text(),'Электронная почтa')]/following-sibling::*/div[@class='profile-info-l-i-text']")
    private WebElement emailValue;

    @FindBy(xpath = "//div[contains(text(),'Телефон')]/following-sibling::*//span[@class='profile-info-l-i-text profile-info-l-i-text-indent']")
    private WebElement phoneNumberValue;

    @FindBy(xpath = "//div[contains(text(),'Адреса для доставок')]/following-sibling::*//ul[@class='profile-info-l']")
    private List<WebElement> deliveryAddressesValueList;

    @FindBy(xpath = "//div[contains(text(),'Дата рождения')]/following-sibling::*//div[@class='profile-info-l-i-text']")
    private WebElement dateOFBirthValue;

    @FindBy(xpath = "//div[contains(text(),'Пол')]/following-sibling::*//div[@class='profile-info-l-i-text']")
    private WebElement genderValue;


    public ProfilePage ensurePageLoaded() {
        By byTextOnPage = By.xpath("//a[contains(text(),'Редактировать личные данные')]");
        super.ensurePageLoaded();
        wait.until(visibilityOfElementLocated(byTextOnPage));
        return this;
    }

    public boolean isConfirmEmailButton() {
        return confitmEmailButtonOnPopup.isDisplayed();
    }
}
