package com.rozetka.model;

import java.util.List;

public class User {
    private String name;
    private String email;
    private String password;
    private String phoneNumber;
    private List<String> deliveryAddresses;
    private String dateOFBirth;
    private String gender;
    private boolean authorized = false;

    public String getName() {
        return name;
    }

    public User() {

    }

    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public User setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public List<String> getDeliveryAddresses() {
        return deliveryAddresses;
    }

    public User setDeliveryAddresses(List<String> deliveryAddresses) {
        this.deliveryAddresses = deliveryAddresses;
        return this;
    }

    public String getDateOFBirth() {
        return dateOFBirth;
    }

    public User setDateOFBirth(String dateOFBirth) {
        this.dateOFBirth = dateOFBirth;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public User setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public boolean getISAuthorized() {
        return authorized;
    }

    public User setAuthorized(boolean authorized) {
        this.authorized = authorized;
        return this;
    }


}
