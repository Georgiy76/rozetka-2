package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreditTests extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void creditTests() {
        app.getNavigationHelper().navigateToCreditPage();
        Assert.assertEquals(app.getCommonHelper().sizeOfListCreditRules(), 4);
    }

    @Test
    public void isCreditTermsTableTitleDisplayed() {
        app.getNavigationHelper().navigateToCreditPage();
        Assert.assertTrue(app.getCommonHelper().isCreditTermsTableTitleDisplayed());
    }

    @Test
    public void isItemExist() {
        app.getNavigationHelper().navigateToCreditPage();
        Assert.assertEquals(app.getCommonHelper().getValueFromCreditTable("Кредит с компенсацией до 35% IQ", "Разовая комиссия"), "1,99");
    }

    @Test
    public void isAgeRentAlphaBank() {
        app.getNavigationHelper().navigateToCreditPage();
        Assert.assertEquals(app.getCommonHelper().getAgeBorrowerAlphaBank(), "21-70");
    }
}
