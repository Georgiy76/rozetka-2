package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Filter extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void isSelectedRightFilter() {
        app.getNavigationHelper().navigateToProductListPagePrice10000_12999Filter();
        Assert.assertTrue(app.getCommonHelper().isPrice10000_12999FilterSelected());
    }
}
