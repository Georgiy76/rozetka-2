package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NumberOfProductsOnPageTileView extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void counntOFProductsOnPage() {
        app.getNavigationHelper().navigateToProductListPage();
        Assert.assertEquals(app.getCommonHelper().sizeOfProductListViewTile(), 32);
    }
}
