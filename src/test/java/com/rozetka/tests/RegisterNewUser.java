package com.rozetka.tests;

import com.rozetka.model.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class RegisterNewUser extends TestBase {

    @BeforeMethod
    public void mayBeLogout() {
        app.getNavigationHelper().navigateToMainPage();
        if (app.getUserHelper().isNotLoggedIn()) {
            return;
        }
        app.getUserHelper().logOut();
    }

    @Test
    public void registerNewUser() {
        User user = new User()
                .setEmail("MegaPuxar40@yopmail.com")
                .setName("MegaPuxar");
                //.setPassword("MegaPassword99");
        app.getUserHelper()
                .createNewUser(user);
        Assert.assertEquals(app.getNavigationHelper().getTtitleOfPage(), "ROZETKA — Личные данные | Личный кабинет");
    }
}
