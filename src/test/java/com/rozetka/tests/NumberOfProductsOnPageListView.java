package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NumberOfProductsOnPageListView extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void counntOFProductsOnPage() {
        app.getNavigationHelper().navigateToProductListPage();
        app.getCommonHelper().changeViewToList();
        Assert.assertEquals(app.getCommonHelper().sizeOfProductListViewList(), 16);
    }
}
