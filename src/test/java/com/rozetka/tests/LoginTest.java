package com.rozetka.tests;

import com.rozetka.model.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest extends TestBase {

    @BeforeMethod
    public void mayBeLogout() {
        app.getNavigationHelper().navigateToMainPage();
        if (app.getUserHelper().isNotLoggedIn()) {
            return;
        }
        app.getUserHelper().logOut();
    }

    @Test
    public void login() throws InterruptedException {
        User user = new User()
                .setEmail("MegaPuxar36@yopmail.com")
                .setPassword("MegaPassword99")
                .setName("MegaPuxar");
        app.getUserHelper()
                .logInAs(user);

        Assert.assertTrue(app.getUserHelper().isLoggedInAs(user));
    }
}
