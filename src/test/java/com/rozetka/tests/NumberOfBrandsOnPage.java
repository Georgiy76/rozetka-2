package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NumberOfBrandsOnPage extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void counntOFBrandsOnPage() {
        app.getNavigationHelper().navigateToProductGroupedPage();
        Assert.assertTrue(app.getProductHelper().numberOfBrandsInMainList(8));
    }
}
