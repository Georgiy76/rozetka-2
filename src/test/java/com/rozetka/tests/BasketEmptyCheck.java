package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BasketEmptyCheck extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void basketEmptyCheck() {
        app.getNavigationHelper().navigateToMainPage();
        Assert.assertTrue(app.getProductHelper().isBasketEmpty());
    }
}
