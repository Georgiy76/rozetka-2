package com.rozetka.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.rozetka.variables.TestData.PRODUCT;

public class AddToBasket extends TestBase {

    @BeforeMethod
    public void getHomePage() {
        app.getNavigationHelper().navigateToMainPage();
    }

    @Test
    public void addToBasket() throws InterruptedException {
        app.getNavigationHelper()
                .navigateToSearchResultPage(PRODUCT);
        app.getProductHelper()
                .addFirstProductToBasket();
        Assert.assertTrue(app.getProductHelper().isProductAddedToBasket());
    }
}
