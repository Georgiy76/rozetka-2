package com.rozetka.tests;

import com.rozetka.applogic2.ApplicationManager;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import java.io.IOException;


public class TestBase {
    protected ApplicationManager app;

    @BeforeClass
    public void load() throws IOException {
        app = new ApplicationManager();

    }

    @AfterSuite
    public void teardown() {
        app.stop();
    }


}
